package com.example.learning.joey.dao.mapper;

import com.example.learning.joey.entity.BetDetails;
import java.util.List;

public interface BetDetailsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(BetDetails record);

    BetDetails selectByPrimaryKey(Integer id);

    List<BetDetails> selectAll();

    int updateByPrimaryKey(BetDetails record);
}