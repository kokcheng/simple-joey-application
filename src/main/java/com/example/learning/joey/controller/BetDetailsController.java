package com.example.learning.joey.controller;

import com.example.learning.joey.dao.mapper.BetDetailsMapper;
import com.example.learning.joey.entity.BetDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author KC
 * @date 24/11/2021
 */
@Controller
@Slf4j
public class BetDetailsController {
    @Resource
    private BetDetailsMapper betDetailsMapper;
    @RequestMapping(value = "/index")
    public String index() {
        log.info("INSIDE WOHOOOOO");
        return "index";
    }

    @RequestMapping(value = "/betdetails")
    public String betdetails( Model model) {
        List<BetDetails> betDetails = betDetailsMapper.selectAll();
        model.addAttribute("betDetails",betDetails);
        return "betdetails";
    }
}
