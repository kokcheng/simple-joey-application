package com.example.learning.joey.controller;

import com.example.learning.joey.dao.mapper.BetDetailsMapper;
import com.example.learning.joey.entity.BetDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;

/**
 * @author KC
 * @date 24/11/2021
 */
@Controller
public class PlaceBetController {
    @Resource
    private BetDetailsMapper betDetailsMapper;

    @RequestMapping(value = "/placebet")
    public String placeBetView( Model model) {
        model.addAttribute("formData",new BetDetails());
        return "placebet";
    }

    @PostMapping("/submitBet")
    public String submitBet(@Valid @ModelAttribute("formData") BetDetails formData,BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "placebet";
        }
        formData.setCreateddate(new Date());
        formData.setUpdateddate(new Date());
        betDetailsMapper.insert(formData);
        return "redirect:/betdetails";
    }
}
