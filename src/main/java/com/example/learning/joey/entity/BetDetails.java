package com.example.learning.joey.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BetDetails implements Serializable {
    private Integer id;

    private String transactionid;

    private String name;

    private String gameid;

    private String gamename;

    private String gametype;

    private BigDecimal betamount;

    private BigDecimal winamount;

    private Date createddate;

    private Date updateddate;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid == null ? null : transactionid.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getGameid() {
        return gameid;
    }

    public void setGameid(String gameid) {
        this.gameid = gameid == null ? null : gameid.trim();
    }

    public String getGamename() {
        return gamename;
    }

    public void setGamename(String gamename) {
        this.gamename = gamename == null ? null : gamename.trim();
    }

    public String getGametype() {
        return gametype;
    }

    public void setGametype(String gametype) {
        this.gametype = gametype == null ? null : gametype.trim();
    }

    public BigDecimal getBetamount() {
        return betamount;
    }

    public void setBetamount(BigDecimal betamount) {
        this.betamount = betamount;
    }

    public BigDecimal getWinamount() {
        return winamount;
    }

    public void setWinamount(BigDecimal winamount) {
        this.winamount = winamount;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    public Date getUpdateddate() {
        return updateddate;
    }

    public void setUpdateddate(Date updateddate) {
        this.updateddate = updateddate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", transactionid=").append(transactionid);
        sb.append(", name=").append(name);
        sb.append(", gameid=").append(gameid);
        sb.append(", gamename=").append(gamename);
        sb.append(", gametype=").append(gametype);
        sb.append(", betamount=").append(betamount);
        sb.append(", winamount=").append(winamount);
        sb.append(", createddate=").append(createddate);
        sb.append(", updateddate=").append(updateddate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}