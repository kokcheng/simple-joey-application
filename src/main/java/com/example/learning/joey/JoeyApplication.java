package com.example.learning.joey;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JoeyApplication {

	public static void main(String[] args) {
		SpringApplication.run(JoeyApplication.class, args);
	}

}
